DROP database IF EXISTS gicdt;
create database gicdt;
	use gicdt;

	create table pais(
		id 			int  unsigned  primary key auto_increment,
		nombre 		varchar(30) not null
		);

	insert into pais values(1,'Afghanistan'),(2,'Algeria'),(3,'American Samoa'),(4,'Angola'),(5,'Anguilla'),(6,'Argentina'),(7,'Armenia'),(8,'Australia'),(9,'Austria'),(10,'Azerbaijan'),(11,'Bahrain'),(12,'Bangladesh'),(13,'Belarus'),(14,'Bolivia'),(15,'Brazil'),(16,'Brunei'),(17,'Bulgaria'),(18,'Cambodia'),(19,'Cameroon'),(20,'Canada'),(21,'Chad'),(22,'Chile'),(23,'China'),(24,'Colombia'),(25,'Congo, The Democratic Republic of the'),(26,'Czech Republic'),(27,'Dominican Republic'),(28,'Ecuador'),(29,'Egypt'),(30,'Estonia'),(31,'Ethiopia'),(32,'Faroe Islands'),(33,'Finland'),(34,'France'),(35,'French Guiana'),(36,'French Polynesia'),(37,'Gambia'),(38,'Germany'),(39,'Greece'),(40,'Greenland'),(41,'Holy See (Vatican City State)'),(42,'Hong Kong'),(43,'Hungary'),(44,'India'),(45,'Indonesia'),(46,'Iran'),(47,'Iraq'),(48,'Israel'),(49,'Italy'),(50,'Japan'),(51,'Kazakstan'),(52,'Kenya'),(53,'Kuwait'),(54,'Latvia'),(55,'Liechtenstein'),(56,'Lithuania'),(57,'Madagascar'),(58,'Malawi'),(59,'Malaysia'),(60,'Mexico'),(61,'Moldova'),(62,'Morocco'),(63,'Mozambique'),(64,'Myanmar'),(65,'Nauru'),(66,'Nepal'),(67,'Netherlands'),(68,'New Zealand'),(69,'Nigeria'),(70,'North Korea'),(71,'Oman'),(72,'Pakistan'),(73,'Paraguay'),(74,'Peru'),(75,'Philippines'),(76,'Poland'),(77,'Puerto Rico'),(78,'Romania'),(79,'Runion'),(80,'Russian Federation'),(81,'Saint Vincent and the Grenadines'),(82,'Saudi Arabia'),(83,'Senegal'),(84,'Slovakia'),(85,'South Africa'),(86,'South Korea'),(87,'Spain'),(88,'Sri Lanka'),(89,'Sudan'),(90,'Sweden'),(91,'Switzerland'),(92,'Taiwan'),(93,'Tanzania'),(94,'Thailand'),(95,'Tonga'),(96,'Tunisia'),(97,'Turkey'),(98,'Turkmenistan'),(99,'Tuvalu'),(100,'Ukraine'),(101,'United Arab Emirates'),(102,'United Kingdom'),(103,'United States'),(104,'Venezuela'),(105,'Vietnam'),(106,'Virgin Islands, U.S.'),(107,'Yemen'),(108,'Yugoslavia'),(109,'Zambia');
		create table estados(
			id 			int unsigned primary key	 auto_increment,
			nombre 		varchar(20) not null,
			abrev		varchar(30),
			id_pais     int unsigned,
			constraint fk_pais_estado foreign key(id_pais) references pais(id)
			);

		insert into estados values(1,'Aguascalientes','Ags.',60),(2,'Baja California','BC',60),(3,'Baja California Sur','BCS',60),(4,'Campeche','Camp.',60),(5,'Coahuila de Zaragoza','Coah.',60),(6,'Colima','Col.',60),(7,'Chiapas','Chis.',60),(8,'Chihuahua','Chih.',60),(9,'Distrito Federal','DF',60),(10,'Durango','Dgo.',60),(11,'Guanajuato','Gto.',60),(12,'Guerrero','Gro.',60),(13,'Hidalgo','Hgo.',60),(14,'Jalisco','Jal.',60),(15,'México','Mex.',60),(16,'Michoacán de Ocampo','Mich.',60),(17,'Morelos','Mor.',60),(18,'Nayarit','Nay.',60),(19,'Nuevo León','NL',60),(20,'Oaxaca','Oax.',60),(21,'Puebla','Pue.',60),(22,'Querétaro','Qro.',60),(23,'Quintana Roo','Q. Roo',60),(24,'San Luis Potosí','SLP',60),(25,'Sinaloa','Sin.',60),(26,'Sonora','Son.',60),(27,'Tabasco','Tab.',60),(28,'Tamaulipas','Tamps.',60),(29,'Tlaxcala','Tlax.',60),(30,'Veracruz de Ignacio de la Llave','Ver.',60),(31,'Yucatán','Yuc.',60),(32,'Zacatecas','Zac.',60);
			create table sectores(
				id 				int unsigned primary key auto_increment,
				nombre 			varchar(70) not null,
				fec_registro    timestamp
				);
			create table organizacion(
				id 				int unsigned primary key auto_increment,
				nombre          varchar(70) not null,
				id_sector		int unsigned,
				constraint fk_sector_organizacion foreign key(id_sector) references sectores(id)
				);
			

			create table usuario(
				id 				int unsigned primary key auto_increment,
				nombre 			varchar(30) not null,
				ap_pat 			varchar(20) not null,
				ap_mat 			varchar(20),
				email   			varchar(25) unique  not null,
				telefono    	varchar(14),
				estado_civil	varchar(14),
				password   			varchar(200) not null,
				img	   			varchar(30) default 'default.png',
				id_facebook 	varchar(40),
				remember_token    varchar(100),
				progreso 			tinyint,
				status 				boolean default 0,
				token_				varchar(100),
				updated_at     timestamp,
				created_at		timestamp,
				cp  			int unsigned,
				tipo            enum('Super','Admin','Cliente') default 'Cliente'				);


INSERT INTO `usuario` (`id`, `nombre`, `ap_pat`, `ap_mat`, `email`, `telefono`, `estado_civil`, `password`, `img`, `id_facebook`, `remember_token`, `progreso`, `status`, `token_`, `updated_at`, `created_at`, `cp`, `tipo`) VALUES
(1, 'Manuel de Jesus', 'Toala', 'Perez', 'toa.l@hotmail.com', NULL, NULL, '$2y$10$gajVMH0IRkaUw9bRFjf4X.zQwwc6ob7Cj6IO0pq3DYQ4eDX/iEkGq', 'default.png', NULL, NULL, NULL, 1, 'VO2H734urWksqMh4KeADQAVVD8b1OZolQamXQe6P7AnQENulqIpt5NTm6fDzqDdtrerleTqZrfng4oZX', '2014-12-05 12:12:25', '2014-12-05 12:12:07', NULL, 'Cliente');



			create table cursos(
				id 			   int unsigned primary key auto_increment,
				nombre 		   varchar(50) not null,
				lugar		   varchar(200) not null,
				descr   		text not null,
				fechas   		varchar(500),
				fecha_registro timestamp,
				id_user        int unsigned,
				cupo		   tinyint not null,
				temario 	   varchar(40),
				logo		   varchar(40),
				tipo           varchar(40),
				nombramiento   varchar(50),
				keywords	   varchar(200),
				couta		   decimal(6,2),
				status		   enum('0','1','2','3') default '0',
				updated_at timestamp,
				created_at timestamp,
				constraint  fk_user_curso   foreign key(id_user) references usuario(id)
				);


			create table user_curso(
				id_curso  			int unsigned,
				id_usuario 			int unsigned,
				fecha_registro		timestamp,
				status 				enum('0','1') default '0',
				constraint fk_curso_user_curso foreign key(id_curso) references cursos(id),
				constraint fk_user_curso_cusro foreign key(id_usuario) references usuario(id)
				);
			create table identidad(
				id int unsigned primary key auto_increment,
				id_user 			int unsigned,
				fec_nac 			date,
				nacionalidad		varchar(20),
				sexo				  enum('M','F'),
				id_pais				int unsigned,
				estado_nacimiento	varchar(100),
				constraint fk_identi_pais_ 	foreign key(id_pais) references pais(id),
				constraint fk_identi_user	foreign key(id_user) references usuario(id)
				);
############################
#Tabla para guardar los documentos de la identidad
############################
create table doc_identidad(
	id    			 int unsigned primary key auto_increment,
	id_pais 		 int unsigned,
	descripcion		 varchar(200),
	doc_principal	 boolean,
	clave_doc		 varchar(100),
	id_usr 			 int unsigned,
	updated_at     timestamp,
	created_at		timestamp,
	constraint fk_docs_user 	foreign key(id_usr) 		references usuario(id),
	constraint fk_doc_pais 		foreign key(id_pais) 	  references pais(id)
);
create table adscripcion(
	id 					int unsigned primary key auto_increment,
	id_organizacion		int unsigned,
	nivel				varchar(10),
	fec_ini				date not null,
	fec_final			date not null,
	nombramiento		varchar(30),
	id_user				int unsigned,
	constraint fk_adscripcion_user			 foreign key (id_user) references usuario(id),
	constraint fk_adscripcion_organizacion	 foreign key (id_organizacion) references organizacion(id)
);
create table distinciones(
	id 			int unsigned primary key auto_increment,
	titulo		varchar(30) not null,
	fecha		date not null,
	id_pais     int unsigned,
	otorgante	varchar(50) not null,
	institucion varchar(100),
	descripcion varchar(300),
	id_user     int unsigned,
	doc   		varchar(100),
	constraint fk_distincion_user foreign key(id_user) references usuario(id)
);
create table  reportes_tecnicos(
	id 			int unsigned primary key auto_increment,
	titulo		varchar(50) not null,
	instancia	varchar(200),
	descripcion text,
	fecha		date,
	objetivo	varchar(200),
	autores		varchar(500),
	keywords	varchar(400),
	id_user 	int unsigned,
	file		varchar(200),
	img			varchar(400),
	updated_at     timestamp,
	created_at		timestamp,
	no_pagina	int,

	constraint fk_reporte_tecnico_user foreign key(id_user) references usuario(id)
);

create table eventos(
	id      int unsigned auto_increment primary key,
	fecha 	timestamp,
	nombre 	 varchar(200),
	descripcion text,
	img 		varchar(50),
	lugar		varchar(400),
	updated_at     timestamp,
	created_at		timestamp
);
create table notas(
	id  int unsigned	auto_increment primary key,
	title		varchar(100),
	content		text,
	id_user  int unsigned,
	updated_at     timestamp,
	created_at		timestamp,
	constraint fk_notas_user foreign key(id_user) references usuario(id)
);
create table notificiones(
	id 		int unsigned auto_increment primary key,
	subjet	varchar(200),
	content  text,
	files	varchar(200),
	para 	varchar(2000),
	de int unsigned,
	updated_at     timestamp,
	created_at		timestamp,
	constraint fk_user_envia foreign key(de) references usuario(id)
);
